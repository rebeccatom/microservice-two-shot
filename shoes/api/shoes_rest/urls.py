from django.urls import path
from .views import api_shoes, show_shoe


urlpatterns = [
    path("shoes/", api_shoes, name="api_shoes"),
    path("shoes/<int:id>/", show_shoe, name="show_shoe"),
]
