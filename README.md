# Wardrobify

Team:

- Rebecca Tom - Hats
- Nolan Michtavy - Shoes

## Design

We chose a modern design for our wardrobe microservice. This entails muted colors, subtle animations (page transitions, hover pseudo classes, etc., adding to the ease of use so it's easy on the eyes), slightly rounded corners (softer on the eyes), sans-serif font type.

## Shoes microservice

Create a shoe model that will include manufacturer, model name, color, picture_url,
Create a bin model that will have a bin name and id

The bin model has a one to many relationship with the shoe model, so to access shoes in the wardrobe you have to first access the bin its located in.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The Hats microservice will contain the following models:

- Hats, which has the attributes: fabric, style name, color, picture URL, and a location in the wardrobe.

Other notes:

- Location has a one-to-many relationship with hats.
- To get to any hat, you have to go through the location in the wardrobe.
