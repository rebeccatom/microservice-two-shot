import React, { useState } from 'react';

export default function HatForm() {
    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const [style, setStyle] = useState('');
    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const [url, setUrl] = useState('');
    const handleUrlChange = (event) => {
        const value = event.target.value;
        setUrl(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.style_name = style
        data.fabric = fabric
        data.hat_color = color
        data.hat_url = url
        data.location = location

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {

            setFabric('');
            setStyle('');
            setColor('');
            setUrl('');
            setLocation('');
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat!</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={style} onChange={handleStyleChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={url} onChange={handleUrlChange} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
                            <label htmlFor="picture">Picture</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={location} onChange={handleLocationChange} placeholder="Location" required type="number" name="location" id="location" className="form-control" />
                            <label htmlFor="location">Location</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
