import { Link } from 'react-router-dom';
import { loadShoesAndHats } from './index'

async function onDelete (event, shoe) {
    event.preventDefault();
    const shoeUrl = `http://localhost:8080/api/shoes/${shoe}/`
    const fetchConfig = {
    method: "delete",
    }
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
        loadShoesAndHats();
    }
    }

function ShoeList(props) {

    if (props.shoes === undefined) {
        return null;
    }

    return (
        <>
        <table className="table table-striped table-hover">
            <thead>
            <tr>
                <th className="text-center">Manufacturer</th>
                <th className="text-center">Model Name</th>
                <th className="text-center">Color</th>
                <th className="text-center">Picture</th>
                <th className="text-center">Bin</th>
            </tr>
            </thead>
            <tbody>
            { props.shoes.map(shoe => {
            return (
            <tr key={shoe.id}>
                <td className="text-center">{ shoe.manufacturer }</td>
                <td className="text-center">{ shoe.model_name }</td>
                <td className="text-center">{ shoe.color }</td>
                <td>
                    <img className="card img-fluid" width="100" height="82" src={shoe.picture_url} alt=""></img>
                </td>
                <td className="text-center">{ shoe.bin.bin_number }</td>
                <td><button onClick={(event) => onDelete(event, shoe.id)} className="btn btn-danger">DELETE</button></td>
            </tr>
            );
            })}
            </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a shoe!</Link>
        </div>
        </>
    );
}

export default ShoeList;
