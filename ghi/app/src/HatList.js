import { Link } from 'react-router-dom';
import { loadShoesAndHats } from './index'

async function onDelete (event, hat) {
    event.preventDefault();
    const hatUrl = `http://localhost:8090/api/hats/${hat}/`
    const fetchConfig = {
    method: "delete",
    }
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
        loadShoesAndHats();
    }
    }

    function HatList(props) {
    return (
        <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th className="text-center">Style Name</th>
            <th className="text-center">Fabric</th>
            <th className="text-center">Color</th>
            <th className="text-center">Picture</th>
            <th className="text-center">Shelf, Section</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map((hat) => {
            return (
              <tr key={hat.id}>
                <td className="text-center">{hat.style_name}</td>
                <td className="text-center">{hat.fabric}</td>
                <td className="text-center">{hat.hat_color}</td>
                <td className="text-center">
                    <img className="card image-fluid" width="100" height="82" src={hat.hat_url} alt=""></img>
                </td>
                <td className="text-center">{hat.location.shelf_number},{hat.location.section_number}</td>
                <td className="text-center"><button className="btn btn-danger" onClick={(event) => onDelete(event, hat.id)}>DELETE</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a hat!</Link>
        </div>
      </>
    );
  }

  export default HatList;
