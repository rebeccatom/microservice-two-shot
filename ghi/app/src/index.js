import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App'

const root = ReactDOM.createRoot(document.getElementById('root'));

export async function loadShoesAndHats() {
  const shoeUrl = "http://localhost:8080/api/shoes";
  const hatUrl = "http://localhost:8090/api/hats"
  const responseShoe = await fetch(shoeUrl)
  const responseHat = await fetch(hatUrl)
  if (responseShoe.ok && responseHat.ok) {
    const dataShoe = await responseShoe.json();
    const dataHat = await responseHat.json()
    root.render(
      <React.StrictMode>
        <App shoes={dataShoe.shoes} hats={dataHat.hats}/>
      </React.StrictMode>
    );
  } else {
    console.error(responseShoe); console.error(responseHat);
  }
}
loadShoesAndHats();
