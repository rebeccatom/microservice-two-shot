import React, { useState } from "react";

function LocationForm() {
    const [closet, setCloset] = useState('');
    const handleClosetChange = (event) => {
        const value = event.target.value;
        setCloset(value);
    }

    const [shelf, setShelf] = useState('');
    const handleShelfChange = (event) => {
        const value = event.target.value;
        setShelf(value);
    }

    const [section, setSection] = useState('');
    const handleSectionChange = (event) => {
        const value = event.target.value;
        setSection(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.closet_name = closet
        data.shelf_number = shelf
        data.section_number = section

        const locationUrl = 'http://localhost:8100/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {

            setCloset('');
            setShelf('');
            setSection('');
        }

    }
    return (
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input value={closet} onChange={handleClosetChange} placeholder="Closet Name" required type="text" name="closet" id="closet_name" className="form-control" />
                            <label htmlFor="closet">Closet Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={shelf} onChange={handleShelfChange} placeholder="Shelf Number" required type="number" name="shelf_number" id="shelf_number" className="form-control" />
                            <label htmlFor="room_count">Room count</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={section} onChange={handleSectionChange} placeholder="Section" required type="number" name="section" id="section" className="form-control" />
                            <label htmlFor="city">Section</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}

export default LocationForm;
